//
//  AppDelegate.h
//  BYHealth_iOS
//
//  Created by panshen on 2017/9/18.
//  Copyright © 2017年 panshen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

