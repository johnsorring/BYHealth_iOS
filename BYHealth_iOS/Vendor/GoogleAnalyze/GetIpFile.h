//
//  GetIpFile.h
//  baobaotong
//
//  Created by lk on 16/5/5.
//  Copyright © 2016年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetIpFile : NSObject
+ (NSString *)getIPAddress;
@end
