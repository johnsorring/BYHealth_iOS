//
//  IPDetector.h
//  WhatIsMyIP
//
//  Created by zzy on 16-01-21.
//  Copyright (c) 2016年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IPDetector : NSObject


+ (void)getLANIPAddressWithCompletion:(void (^)(NSString *IPAddress))completion;

+ (void)getWANIPAddressWithCompletion:(void(^)(NSString *IPAddress))completion;

+ (NSString *)getIPAddress;

+ (NSString *)getDate;

+ (void)creatScreenAnalytics:(NSString *)screenName;
+ (void) signInWithUser;
+ (void)creatTargetCategory:(NSString *)category withAction:(NSString *)action withValue:(NSNumber *)value;
+ (void)creatTargetCategory:(NSString *)category withAction:(NSString *)action withLable:(NSString *)lab;
@end
