//
//  IPDetector.m
//  WhatIsMyIP
//
//  Created by zzy on 16-01-21.
//  Copyright (c) 2016年 zzy. All rights reserved.
//

#import "IPDetector.h"
#include <ifaddrs.h>
#include <arpa/inet.h>


@implementation IPDetector


+ (void)getLANIPAddressWithCompletion:(void (^)(NSString *IPAddress))completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *IP = [self getIPAddress];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion(IP);
            }
        });
    });
}

+ (void)getWANIPAddressWithCompletion:(void(^)(NSString *IPAddress))completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *IP = @"0.0.0.0";
        NSURL *url = [NSURL URLWithString:@"http://ifconfig.me/ip"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:8.0];
        
        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (error) {
            NSLog(@"Failed to get WAN IP Address!\n%@", error);
            //            [[[UIAlertView alloc] initWithTitle:@"获取外网 IP 地址失败" message:[error localizedFailureReason] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else {
            NSString *responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            IP = responseStr;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(IP);
        });
    });
}

#pragma mark -
// http://zachwaugh.me/posts/programmatically-retrieving-ip-address-of-iphone/
+ (NSString *)getIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if( temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    
    return address;
}

#pragma mark 获取当前时间
+ (NSString *)getDate {
    NSDateFormatter*formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *locationString=[formatter stringFromDate: [NSDate date]];
    return locationString;
}

#pragma mark -- 监测屏幕
+ (void)creatScreenAnalytics:(NSString *)screenName {
    if ([googleAnalysis isEqualToString:@"isopen"]) {
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:screenName];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
}
//加入user_Id 只需要在程序执行的时候使用一次
+ (void) signInWithUser{
    if ([googleAnalysis isEqualToString:@"isopen"]) {
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        // You only need to set User ID on a tracker once. By setting it on the tracker, the ID will be
        // sent with all subsequent hits.
        [tracker set:kGAIUserId
               value:[Account currentAccount].userinfo[@"userid"]];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"add-userid"
                                                              action:@"User-Sign-In"
                                                               label:nil
                                                               value:nil] build]];
    }
}
#pragma mark -- 类别&事件
+ (void)creatTargetCategory:(NSString *)category withAction:(NSString *)action withValue:(NSNumber *)value{
    if ([googleAnalysis isEqualToString:@"isopen"]) {
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                        // Event category (required)
                                                              action:action             // Event action (required)
                                                               label:nil                // Event label
                                                               value:value] build]];      // Event value
    }
}
+ (void)creatTargetCategory:(NSString *)category withAction:(NSString *)action withLable:(NSString *)lab{
    if ([googleAnalysis isEqualToString:@"isopen"]) {
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                        // Event category (required)
                                                              action:action             // Event action (required)
                                                               label:lab              // Event label
                                                               value:nil] build]];      // Event value
    }
}

@end
