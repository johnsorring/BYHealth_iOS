//
//  JavaOCInterRac.m
//  baobaotong
//
//  Created by lk on 16/7/14.
//  Copyright © 2016年 zzy. All rights reserved.
//

#import "JavaOCInterRac.h"
#import <UIKit/UIKit.h>
#import <JavaScriptCore/JavaScriptCore.h>



//首先创建一个实现了JSExport协议的协议
@protocol JavaOCInterRacProtocol <JSExport>
-(void)loadFinish;
-(void)getTitle:(NSString *)title;
-(void)showHintMsg:(NSString *)str;
-(void)showTipsDialog:(NSString *)message;
-(void)switchEvent:(NSString *)eventType;
-(void)getImageInfor:(NSString *)imageUrl :(NSString *)imageType;
-(void)synchronousStudy;
-(void)updateCoursePraxis;
-(void)scrollChange:(BOOL)istop;
-(void)uploadStudySuccess:(NSString *)string;
-(void)shareParams:(NSString *)title :(NSString *)sharepath :(NSString *)sharecontent;
-(void)showBackTipsDialog:(NSString *)message :(NSString *)isfinish;
-(void)showCommentBox;
-(void)closeSimulationTest;
-(void)call:(NSString *)phone;
-(void)sendMsg:(NSString *)msg :(NSString *)phone;
-(void)share:(NSString *)url :(NSString *)title :(NSString *)content :(NSString *)headurl;
-(void)pageBack:(BOOL)isTop;
-(void)setTitle:(NSString *)title;
#pragma marks -----新展业秀
-(void)menuBar:(BOOL)isShowing; //是否显示菜单栏
-(void)menuBarStatus:(BOOL)isShowing; //是否显示菜单栏
-(void)changeFace; //点击换肤 换完皮肤后调用js的currentFace(String id)
-(void)editData;

-(void)toMRQD;//每日签到 到每日签到
-(void)toSFRZ;//到身份认证  到个人资料
-(void)toNC;//到昵称
-(void)toGRJJ;//到个人简介
-(void)toCPFA;//到产品方案
-(void)toKTKJDL;//开通快捷登录
-(void)toZYXFX;//到展业秀
-(void)uploadRateFile;//进入文件上传

-(void)toExperience:(NSString *)code; //新手专区立即体验

-(void)getDeviceInfor; //获取设备信息

// 积分任务-每日任务
- (void)toZX; //到资讯 资讯分享/资讯评论
- (void)toBBWD; //到保宝问答 关注问题/回答问题
- (void)toYQHY; //邀请好友
- (void)toTK; //到题库 每日一练
- (void)aboutIntegral: (NSString *)infostate :(NSString *)acquiredscore :(NSString *)remindwords; // 是否是第一次分享/评论等

@end
@interface JavaOCInterRac() <JavaOCInterRacProtocol,UIAlertViewDelegate>

@end
@implementation JavaOCInterRac

//static JavaOCInterRac *JavaOCInterac = nil;
//+(instancetype)getInstance
//{
//    
//    @synchronized(self)
//    {
//        if(!JavaOCInterac)
//        {
//            JavaOCInterac = [[self alloc] init];
//        }
//    }
//    return JavaOCInterac;
//}

-(void)showTipsDialog:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"返回检查" otherButtonTitles:@"确认提交", nil];
        alert.tag = 10010;
        [alert show];
    });
    
}

-(void)closeSimulationTest
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"close-Test" object:nil userInfo:nil];
}

-(void)showBackTipsDialog:(NSString *)message :(NSString *)isfinish
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"返回结束" otherButtonTitles:@"继续做题", nil];
        alert.tag = 10011 + [isfinish integerValue] * 10;
        [alert show];
    });
}

-(void)updateCoursePraxis
{
    
}

-(void)uploadStudySuccess:(NSString *)string
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:string delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alert show];
    });
}

-(void)loadFinish
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadFinish" object:nil userInfo:nil];
}

-(void)getImageInfor:(NSString *)imageUrl :(NSString *)imageType
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"imageUrl"] = imageUrl;
    dic[@"imageType"] = imageType;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadPic" object:nil userInfo:dic];
}

-(void)getTitle:(NSString *)title
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"title"] = title;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"get-title" object:nil userInfo:dic];
}

-(void)scrollChange:(BOOL)istop
{
    if (istop) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"can-drag" object:nil userInfo:nil];
    }else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"cannot-drag" object:nil userInfo:nil];
    }
}

-(void)showCommentBox
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showCommentBox" object:nil userInfo:nil];
}

-(void)showHintMsg:(NSString *)str
{
    dispatch_queue_t queue = dispatch_get_main_queue();
    dispatch_async(queue, ^{
//        [HeadTapGo tub:NO message:str];
    });
    
}

-(void)switchEvent:(NSString *)eventType
{
    if ([eventType isEqualToString:@"Single-choice"]) {
        //单选
        [IPDetector creatTargetCategory:@"题型选择" withAction:@"Single-choice" withValue:nil];
    }else if([eventType isEqualToString:@"Multiple-choice"])
    {
        //多选
        [IPDetector creatTargetCategory:@"题型选择" withAction:@"Multiple-choice" withValue:nil];
    }else if([eventType isEqualToString:@"True-false"])
    {
        //判断
        [IPDetector creatTargetCategory:@"题型选择" withAction:@"True-false" withValue:nil];
    }else if([eventType isEqualToString:@"Restart"])
    {
        //重新开始
        [IPDetector creatTargetCategory:@"测试结果" withAction:@"Restart" withValue:nil];
    }else if([eventType isEqualToString:@"Answer-card0"])
    {
        //做题－答题卡
        [IPDetector creatTargetCategory:@"答题卡" withAction:@"Answer-card0" withValue:nil];
    }else if([eventType isEqualToString:@"Answer-card1"])
    {
        //解析答题卡
        [IPDetector creatTargetCategory:@"答题卡" withAction:@"Answer-card1" withValue:nil];
    }else if([eventType isEqualToString:@"Check-analysis"])
    {
        //查看解析
        [IPDetector creatTargetCategory:@"测试结果" withAction:@"Check-analysis" withValue:nil];
    }else if([eventType isEqualToString:@"Look-answer"])
    {
        [IPDetector creatScreenAnalytics:@"Exercises-after-analysis"];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10010) {
        if (buttonIndex == 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"goback-study" object:nil userInfo:nil];
        }else if (buttonIndex == 1)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"goahead-study" object:nil userInfo:nil];
        }
    }else if (alertView.tag == 10021)
    {
        if (buttonIndex == 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"back-end" object:nil userInfo:nil];
        }
    }else if (alertView.tag == 10011)
    {
        if (buttonIndex == 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"self-control" object:nil userInfo:nil];
        }
    }
}

-(void)synchronousStudy
{
    
}

-(void)shareParams:(NSString *)title :(NSString *)sharepath :(NSString *)sharecontent
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"title"] = title;
    params[@"sharepath"] = sharepath;
    params[@"sharecontent"] = sharecontent;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"params" object:nil userInfo:params];
    
}

-(void)call:(NSString *)phone
{
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

-(void)sendMsg:(NSString *)msg :(NSString *)phone
{
    
}

-(void)share:(NSString *)url :(NSString *)title :(NSString *)content :(NSString *)headurl
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"title"] = title;
    params[@"sharepath"] = url;
    params[@"sharecontent"] = content;
    params[@"imgurl"] = headurl;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"share-self" object:nil userInfo:params];
}

-(void)pageBack:(BOOL)isTop
{
    
}

-(void)setTitle:(NSString *)title
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"title"] = title;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reset-title" object:nil userInfo:params];
}

-(void)menuBar:(BOOL)isShowing
{
    
}

-(void)changeFace
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"change-face" object:nil userInfo:nil];
}

-(void)menuBarStatus:(BOOL)isShowing
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (!isShowing) {
        params[@"isshow"] = @"0";
    }else
    {
        params[@"isshow"] = @"1";
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"show-left" object:nil userInfo:params];
}

-(void)editData
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"edit-data" object:nil userInfo:nil];
}

-(void)toMRQD//每日签到 到每日签到
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toMRQD" object:nil userInfo:nil];
}

-(void)toSFRZ//到身份认证  到个人资料
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toSFRZ" object:nil userInfo:nil];
}

-(void)toNC//到昵称
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toNC" object:nil userInfo:nil];
}

-(void)toGRJJ//到个人简介
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toGRJJ" object:nil userInfo:nil];
}

-(void)toCPFA//到产品方案
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toCPFA" object:nil userInfo:nil];
}

-(void)toKTKJDL//开通快捷登录
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toKTKJDL" object:nil userInfo:nil];
}

-(void)toZYXFX//到展业秀
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toZYXFX" object:nil userInfo:nil];
}

-(void)uploadRateFile
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UploadRateFile" object:nil userInfo:nil];
}

-(void)toExperience:(NSString *)code //新手专区立即体验
{
    NSDictionary *dic = @{@"code":code};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ToExperience" object:nil userInfo:dic];
}

-(void)getDeviceInfor
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateApp_Vision" object:nil userInfo:nil];
}

- (void)toZX {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toZX" object:nil userInfo:nil];
}

- (void)toBBWD {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toBBWD" object:nil userInfo:nil];
}

- (void)toYQHY {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toYQHY" object:nil userInfo:nil];
}

- (void)toTK {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toTK" object:nil userInfo:nil];
}

- (void)aboutIntegral: (NSString *)infostate :(NSString *)acquiredscore :(NSString *)remindwords {
    
    if (self.aboutIntegralBlock) {
        self.aboutIntegralBlock(infostate, acquiredscore, remindwords);
    }
}

@end
