//
//  JavaOCInterRac.h
//  baobaotong
//
//  Created by lk on 16/7/14.
//  Copyright © 2016年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JavaScriptCore/JavaScriptCore.h>


@interface JavaOCInterRac : NSObject

@property(nonatomic, copy) NSString *titleStr;
@property(nonatomic, copy) void (^aboutIntegralBlock)(NSString * infostate, NSString *acquiredscore, NSString *remindwords);
//+(instancetype)getInstance;

@end
