//
//  UITextField+ExtentRange.h
//  baobaotong
//
//  Created by lk on 2017/6/20.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (ExtentRange)

- (NSRange) selectedRange;
- (void) setSelectedRange:(NSRange) range;
@end
