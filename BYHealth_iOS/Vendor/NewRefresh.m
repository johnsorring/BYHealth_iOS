//
//  NewRefresh.m
//  baobaotong
//
//  Created by 李康 on 2017/2/23.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "NewRefresh.h"

@implementation NewRefresh
- (void)prepare
{
    [super prepare];
    //创建UIImageView
    // 设置普通状态的动画图片
    NSMutableArray *idleImages = [NSMutableArray array];
    
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"refresh_disable"]];
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    UIImageView * imagV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    imagV.image = image;
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, height - 20 - 5, width, 20)];
    lab.font = [UIFont systemFontOfSize:12];
    lab.textAlignment = NSTextAlignmentCenter;
    lab.textColor = UIColorFromRGB(0x4B4B4B);
    lab.text = @"下拉即可刷新……";
    [imagV addSubview:lab];
    UIImage *newimage = [self convertViewToImage:imagV];
    [idleImages addObject:newimage];
    [self setImages:idleImages forState:MJRefreshStateIdle];
    
    // 设置即将刷新状态的动画图片（一松开就会刷新的状态）
    NSMutableArray *refreshingImages = [NSMutableArray array];
    UIImage *image1 = [UIImage imageNamed:[NSString stringWithFormat:@"refresh_enable"]];
    CGFloat width1 = image1.size.width;
    CGFloat height1 = image1.size.height;
    UIImageView * imagV1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width1, height1)];
    imagV1.image = image1;
    UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(0, height1 - 20 - 5, width1, 20)];
    lab1.font = [UIFont systemFontOfSize:12];
    lab1.textAlignment = NSTextAlignmentCenter;
    lab1.textColor = UIColorFromRGB(0x4B4B4B);
    lab1.text = @"松手即可刷新……";
    [imagV1 addSubview:lab1];
    UIImage *newimage1 = [self convertViewToImage:imagV1];
    [refreshingImages addObject:newimage1];
    [self setImages:refreshingImages forState:MJRefreshStatePulling];
    
    // 设置正在刷新状态的动画图片
    [self setImages:refreshingImages forState:MJRefreshStateRefreshing];
    //隐藏时间
    self.lastUpdatedTimeLabel.hidden = YES;
    //    隐藏状态
    self.stateLabel.hidden = YES;
    //根据需求设置刷新文字，不同状态的刷新文字都可以自定义
    [self setTitle:@"放开可以刷新" forState:MJRefreshStatePulling];
    
    //    [self setTitle:@"正在刷新" forState:MJRefreshStateRefreshing];
    //    [self setTitle:@"下拉刷新" forState:MJRefreshStateWillRefresh];
    
}
-(UIImage*)convertViewToImage:(UIView*)v{
    CGSize s = v.bounds.size;
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
