//
//  HomeViewController.m
//  BYHealth_iOS
//
//  Created by panshen on 2017/9/18.
//  Copyright © 2017年 panshen. All rights reserved.
//

#import "HomeViewController.h"
#import "BBMaxScrollCell.h"
#import "BBNewPersonCell.h"
#import "BBHeadlineCell.h"
#import "VipCollectionViewLayout.h"

static NSString * const maxScrollCellID = @"maxScrollCell";
static NSString * const newPersonCellID = @"newPersonCell";
static NSString * const headlineCellID = @"headlineCell";

@interface HomeViewController ()
<UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout,
UIGestureRecognizerDelegate>

@property (nonatomic,strong) UIButton *sys;/** 扫一扫 */
@property(nonatomic, strong) UICollectionView *homePageCollectionView;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navBarView.hidden = YES;
    
    [self creatUI];
}


-(void)creatUI{
    
    VipCollectionViewLayout * layout = [[VipCollectionViewLayout alloc] init];
    layout.itemSize = CGSizeMake(kDeviceWidth/4.0, 93.0 * kScreenScaleX);
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    _homePageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight - 49) collectionViewLayout:layout];
    _homePageCollectionView.backgroundColor = [UIColor whiteColor];
    _homePageCollectionView.showsVerticalScrollIndicator = NO;
    _homePageCollectionView.showsHorizontalScrollIndicator = NO;
    _homePageCollectionView.delegate = self;
    _homePageCollectionView.dataSource = self;
//    _homePageCollectionView.mj_header = [NewRefresh headerWithRefreshingTarget:self refreshingAction:@selector(homeHeaderRereshing)];
    [self.view addSubview:_homePageCollectionView];
    
    [self.homePageCollectionView registerClass:[BBMaxScrollCell class] forCellWithReuseIdentifier:maxScrollCellID];
    [self.homePageCollectionView registerClass:[BBNewPersonCell class] forCellWithReuseIdentifier:newPersonCellID];
    [self.homePageCollectionView registerClass:[BBHeadlineCell class] forCellWithReuseIdentifier:headlineCellID];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        BBMaxScrollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:maxScrollCellID forIndexPath:indexPath];
//        cell.picArr = self.picArr;
//        _sdScrollView = cell.scrollView;
        return cell;
    }
    
    else if (indexPath.section == 1) {
        BBNewPersonCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:newPersonCellID forIndexPath:indexPath];
        
        return cell;
    }
    
    else {
        BBHeadlineCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:headlineCellID forIndexPath:indexPath];
        
        return cell;
    }
}

#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize customItemSize;
    
    if (indexPath.section == 0) {
        customItemSize = CGSizeMake(kDeviceWidth, 195.0 * kScreenScaleX);
    }
    
    else if (indexPath.section == 1) {
        customItemSize = CGSizeMake(kDeviceWidth, 122.0 + 48.0 + 8.0);
    }
    
    else {
        customItemSize = CGSizeMake(kDeviceWidth, 152.0 + 48.0 + 8.0);
    }
    
    return customItemSize;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - UICollectionViewDelegateFlowLayout


#pragma mark ----- scrollview代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    CGFloat offsetY = scrollView.contentOffset.y;
//    CGFloat height = kDeviceWidth * 195.0 /375.0;
//    if (offsetY >= height) {
//        
//        [_nav removeSubLay];
//        _nav.backgroundColor = [UIColor whiteColor];
//        _nav.tag = 9998;
//        [_sys setImage:[[UIImage imageNamed:@"ic_home_scan_second_brown"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:0];
//        [_qd setTitleColor:UIColorFromRGB(0xFF9600) forState:0];
//        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
//        
//    } else {
//        
//        _na.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
//        _nav.tag = 9999;
//        NSArray *colorArr = @[(__bridge id)[UIColorFromRGB(0x333333) colorWithAlphaComponent:0.4].CGColor,(__bridge id)[UIColorFromRGB(0x4D4D4D) colorWithAlphaComponent:0.3].CGColor,(__bridge id)[UIColorFromRGB(0x4C4C4C) colorWithAlphaComponent:0.1].CGColor,(__bridge id)[UIColorFromRGB(0xD8D8D8) colorWithAlphaComponent:0.0].CGColor];
//        [_nav reSetSetcolorArrFromStartToEnd:colorArr];
//        [_sys setImage:[[UIImage imageNamed:@"ic_home_scan_second"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:0];
//        [_qd setTitleColor:KwhiteColor forState:0];
//        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
//    }
}


@end
