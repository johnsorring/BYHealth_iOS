//
//  TTFResultModel.h
//  baobaotong
//
//  Created by 顾宏钟 on 2017/6/9.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTFResult.h"
@interface TTFResultModel : NSObject

@property (nonatomic, strong) NSString * javaClass;
@property (nonatomic, strong) NSArray * result;
@property (nonatomic, strong) NSString * resultdesc;
@property (nonatomic, assign) NSInteger resultstate;

@end
