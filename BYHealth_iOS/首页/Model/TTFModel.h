//
//  TTFModel.h
//  baobaotong
//
//  Created by 顾宏钟 on 2017/6/9.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTFModel : NSObject

@property (nonatomic, strong) NSString * beanname;
@property (nonatomic, strong) NSString * linkedtype;
@property (nonatomic, strong) NSString * linkedurl;
@property (nonatomic, strong) NSString * orderindex;
@property (nonatomic, strong) NSString * picpath;
@property (nonatomic, strong) NSString * text;
@property (nonatomic, strong) NSString * uniqueid;

@end
