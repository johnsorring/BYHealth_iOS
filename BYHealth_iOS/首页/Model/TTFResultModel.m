//
//  TTFResultModel.m
//  baobaotong
//
//  Created by 顾宏钟 on 2017/6/9.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "TTFResultModel.h"

@implementation TTFResultModel

+(NSDictionary *)mj_objectClassInArray
{
    return @{@"result":@"TTFResult"};
}

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property
{
    if ([property.name isEqualToString:@"publisher"]) {
        if (oldValue == nil) return @"";
        
    } else if (property.type.typeClass == [NSDate class]) {
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"yyyy-MM-dd";
        return [fmt dateFromString:oldValue];
    }
    
    return oldValue;
}
@end
