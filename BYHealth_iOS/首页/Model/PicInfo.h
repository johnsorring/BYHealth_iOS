//
//  PicInfo.h
//  baobaotong
//
//  Created by lk on 2017/5/22.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PicInfo : NSObject
@property (nonatomic, strong) NSString * beanname;
@property (nonatomic, strong) NSString * linkedtype;
@property (nonatomic, strong) NSString * linkedurl;
@property (nonatomic, strong) NSString * orderindex;
@property (nonatomic, strong) NSString * picpath;
@property (nonatomic, strong) NSString * urltype;
@property (nonatomic, strong) NSString * title;

@end
