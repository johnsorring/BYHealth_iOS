//
//  TTFResult.h
//  baobaotong
//
//  Created by lk on 2017/6/30.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTFModel.h"
@interface TTFResult : NSObject

@property (nonatomic, strong) NSString * areaname;
@property (nonatomic, strong) NSString * beanname;
@property (nonatomic, strong) NSArray  * childmenus;
@property (nonatomic, strong) NSString * loadmoretype;
@property (nonatomic, strong) NSString * orderindex;
@property (nonatomic, strong) NSString * style;
@property (nonatomic, strong) NSString * stylename;
@end
