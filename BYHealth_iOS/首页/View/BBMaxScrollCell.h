//
//  BBMaxScrollCell.h
//  baobaotong
//
//  Created by 顾宏钟 on 2017/5/26.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
@class PicInfo;
@interface BBMaxScrollCell : UICollectionViewCell <SDCycleScrollViewDelegate>

@property(nonatomic, strong) PicInfo *info;
@property(nonatomic, strong) NSArray *picArr;
@property(nonatomic, strong)SDCycleScrollView * scrollView;

@end
