//
//  BBNewPersonChildCell.m
//  baobaotong
//
//  Created by 顾宏钟 on 2017/6/9.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "BBNewPersonChildCell.h"
#import "TTFModel.h"

@implementation BBNewPersonChildCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews {
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0 + 8, self.frame.size.width, self.frame.size.height)];
    [self.contentView addSubview:_imageView];
}

- (void)setNpModel:(TTFModel *)npModel
{
    _npModel = npModel;
    NSLog(@"%@",npModel.picpath);
    [_imageView sd_setImageWithURL:[NSURL URLWithString:npModel.picpath]];
}

@end
