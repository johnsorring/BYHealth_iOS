//
//  BBMaxScrollCell.m
//  baobaotong
//
//  Created by 顾宏钟 on 2017/5/26.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "BBMaxScrollCell.h"
#import "PicInfo.h"

@implementation BBMaxScrollCell

-(instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        [self setupMaxScrollView];
    }
    
    return self;
}

- (void)setupMaxScrollView {
    CGFloat height = kDeviceWidth * 195.0 /375.0;
    //顶部的滚动视图
    _scrollView = [[SDCycleScrollView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, height)];
    _scrollView.delegate = self;
    _scrollView.autoScrollTimeInterval = 5.0;
    _scrollView.currentPageDotImage = [self createImg:[UIImage imageNamed:@"ic_home_indicator_focus"]];
    _scrollView.pageDotImage = [self createImg:[UIImage imageNamed:@"ic_home_indicator_normal"]];
    _scrollView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
    _scrollView.backgroundColor  = [UIColor colorWithRed:192.0/255.0f green:192.0/255.0f blue:192.0/255.0f alpha:0.7];
    _scrollView.placeholderImage = [UIImage imageNamed:@"banner_null"];
    [self.contentView addSubview:_scrollView];
}

- (void)setPicArr:(NSArray *)picArr {
    
    _picArr = picArr;
    
    NSMutableArray *picUrlArr = [NSMutableArray array];
    for (int i = 0; i < picArr.count; i++) {
        _info = _picArr[i];
        [picUrlArr addObject:_info.picpath];
    }
    _scrollView.imageURLStringsGroup = picUrlArr;
}

-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    
}

#pragma mark ----- 进入文件上传到web页面
-(void)goFileUploadWeb:(UIViewController *)contr
{
    
}

-(UIImage *)createImg:(UIImage *)img
{
    UIView *backView = [[UIView alloc] init];
    UIImageView *imgV = [[UIImageView alloc] init];
    CGFloat width = img.size.width;
    CGFloat height = img.size.height;
    if(width != height)
    {
        imgV.frame = CGRectMake(0, 0, 12, 5);
        imgV.image = img;
    }else
    {
        imgV.frame = CGRectMake(0, 0, 6, 6);
        imgV.image = img;
    }
    backView.frame = CGRectMake(0, 0, 12, 6);
    [backView addSubview:imgV];
    imgV.center = backView.center;
    return  [self convertViewToImage:backView];
}

-(UIImage*)convertViewToImage:(UIView*)v{
    CGSize s = v.bounds.size;
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
