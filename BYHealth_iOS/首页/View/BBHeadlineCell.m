//
//  BBHeadlineCell.m
//  baobaotong
//
//  Created by 顾宏钟 on 2017/5/31.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "BBHeadlineCell.h"
#import "BBHeadlineChildLineCell.h"
#import "TTFModel.h"

static NSString * const headlineID = @"headlineCell";
@interface BBHeadlineCell()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property(nonatomic, strong) UIView *headView;
@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) UILabel *titleLabel;
@property (nonatomic,strong) UIButton *moreBtn;
@property(nonatomic, strong) NSArray *headLArr;

@end

@implementation BBHeadlineCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews {
    
    // 新手专区
    _headView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 48 + 8)];
    self.headView.userInteractionEnabled = YES;
    [self.contentView addSubview:_headView];
    UIView *seperateViewTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 8)];
    seperateViewTop.backgroundColor = kBackgroundColor;
    [_headView addSubview:seperateViewTop];
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.frame = CGRectMake(16, (48 - 16.5) * 0.5 + 8, kDeviceWidth - 16 - 85 - 16, 16.5);
    _titleLabel.text = @"宝易健康头条";
    _titleLabel.font = [UIFont systemFontOfSize:12];
    _titleLabel.textColor = [UIColor colorWithRed:77/255.0 green:77/255.0 blue:77/255.0 alpha:1/1.0];
    [_headView addSubview:_titleLabel];
    
    _moreBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _moreBtn.frame = CGRectMake(kDeviceWidth - 85, 8, 85, 48);
    [_headView addSubview:_moreBtn];
    
    UIImageView *imagev =[[UIImageView alloc] initWithFrame:CGRectMake(85 - 24 - 4, 12, 24, 24)];
    imagev.image = [UIImage imageNamed:@"ic_more"];
    [_moreBtn addSubview:imagev];
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(imagev.frame) - 6 - 51, 16, 51, 16.5)];
    lab.text = @"查看更多";
    lab.textColor = UIColorFromRGB(0x4c4c4c);
    lab.font = [UIFont systemFontOfSize:12];
    [_moreBtn addSubview:lab];
    [_moreBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    // 新手工具/指南
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(240, 120);
    layout.minimumLineSpacing = 16;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 48 + 8, kDeviceWidth, 152) collectionViewLayout:layout];
    [self.contentView addSubview:_collectionView];
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.contentInset = UIEdgeInsetsMake(0, 16, 0, 16);
    _collectionView.showsHorizontalScrollIndicator = false;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    // 注册
    [_collectionView registerClass:[BBHeadlineChildLineCell class] forCellWithReuseIdentifier:headlineID];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _headLArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BBHeadlineChildLineCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:headlineID forIndexPath:indexPath];
    cell.headlineModel = _headLArr[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    UIViewController *contr = (UIViewController *)[HeadTapGo currentViewC];
//    if (collectionView == _collectionView && contr) {
//        BBHeadlineChildLineCell *cell = (BBHeadlineChildLineCell *)[collectionView cellForItemAtIndexPath:indexPath];
//        HomeInfoViewController *newc =[[HomeInfoViewController alloc] init];
//        newc.title = cell.headlineModel.text;
//        [IPDetector creatTargetCategory:@"BB-headlines" withAction:[NSString stringWithFormat:@"headlines%ld",(long)(index + 1)] withLable:[NSString stringWithFormat:@"推荐资讯%ld",(long)(index + 1)]];
//        newc.url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@&cardno=%@&userid=%@",[cell.headlineModel.linkedurl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[Account currentAccount].userinfo[@"usersfzh"],[Account currentAccount].userinfo[@"userid"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//        [contr.navigationController pushViewController:newc animated:YES];
//    }
    
}


-(void)btnClick:(UIButton *)btn
{
//    UIViewController *contr = (UIViewController *)[HeadTapGo currentViewC];
//    if (contr) {
//        NewZxViewController *nz = [[NewZxViewController alloc] init];
//        [contr.navigationController pushViewController:nz animated:YES];
//    }
}


@end
