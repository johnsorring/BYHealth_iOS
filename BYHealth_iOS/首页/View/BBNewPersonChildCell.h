//
//  BBNewPersonChildCell.h
//  baobaotong
//
//  Created by 顾宏钟 on 2017/6/9.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TTFModel;

@interface BBNewPersonChildCell : UICollectionViewCell

@property(nonatomic, strong) UIImageView *imageView;
/** 模型属性 */
@property(nonatomic, strong) TTFModel *npModel;

@end
