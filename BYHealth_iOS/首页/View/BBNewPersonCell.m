//
//  BBNewPersonCell.m
//  baobaotong
//
//  Created by 顾宏钟 on 2017/5/27.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "BBNewPersonCell.h"
#import "BBNewPersonChildCell.h"
#import "TTFModel.h"

static NSString * const npChildID = @"npChildCell";
@interface BBNewPersonCell()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

/** 模型数组 */
@property(nonatomic, strong) NSArray *npArr;

@property(nonatomic, strong) UIView *headView;
@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) UILabel *titleLabel;
@end

@implementation BBNewPersonCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews {
    
    // 新手专区
    _headView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 47)];
    [self.contentView addSubview:_headView];
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.frame = CGRectMake(16, (48 - 16.5) * 0.5 * kScreenScaleY, kDeviceWidth - 16 * 2, 16.5 * kScreenScaleY);
    _titleLabel.text = @"";
    _titleLabel.font = [UIFont systemFontOfSize:12];
    _titleLabel.textColor = [UIColor colorWithRed:77/255.0 green:77/255.0 blue:77/255.0 alpha:1/1.0];
    [_headView addSubview:_titleLabel];
    
    // 新手工具/指南
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(160, 100);
    layout.minimumLineSpacing = 16;
    layout.minimumInteritemSpacing = 0;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 48, kDeviceWidth, 122 * kScreenScaleY) collectionViewLayout:layout];
    [self.contentView addSubview:_collectionView];
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.contentInset = UIEdgeInsetsMake(0, 16, 0, 16);
    _collectionView.showsHorizontalScrollIndicator = false;
    
    /** 弹簧效果 */
    _collectionView.bounces = true;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    // 注册
    [_collectionView registerClass:[BBNewPersonChildCell class] forCellWithReuseIdentifier:npChildID];
    UIView *seperateViewBottom = [[UIView alloc] initWithFrame:CGRectMake(0, 48 + 122, kDeviceWidth, 8)];
    seperateViewBottom.backgroundColor = kBackgroundColor;
    [self.contentView addSubview:seperateViewBottom];
}

#pragma mark - UICollectionViewDelegate...
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _npArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BBNewPersonChildCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:npChildID forIndexPath:indexPath];
    cell.npModel = _npArr[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    UIViewController *contr = (UIViewController *)[HeadTapGo currentViewC];
//    if (collectionView == _collectionView && contr) {
//        
//        BBNewPersonChildCell *cell = (BBNewPersonChildCell *)[collectionView cellForItemAtIndexPath:indexPath];
//        NSString *showLab = @"";
//        if (indexPath.row == 0) {
//            showLab = @"展业工具介绍";
//        }else if (indexPath.row == 1)
//        {
//            showLab = @"展业工具介绍";
//        }
//        [IPDetector creatTargetCategory:@"Newbie-zone" withAction:[NSString stringWithFormat:@"Newbie-area%ld",(long)(indexPath.row + 1)] withLable:showLab];
//    }
    
    
}



@end
