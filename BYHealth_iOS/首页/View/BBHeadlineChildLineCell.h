//
//  BBHeadlineChildLineCell.h
//  baobaotong
//
//  Created by 顾宏钟 on 2017/6/6.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
@class TTFModel;

@interface BBHeadlineChildLineCell : UICollectionViewCell

@property(nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong)UILabel *titleLab;
@property (nonatomic,strong)UIView *backTitleView;
/** 模型属性 */
@property(nonatomic, strong) TTFModel *headlineModel;
@end
