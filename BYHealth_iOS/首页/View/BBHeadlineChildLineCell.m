//
//  BBHeadlineChildLineCell.m
//  baobaotong
//
//  Created by 顾宏钟 on 2017/6/6.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "BBHeadlineChildLineCell.h"
#import "TTFModel.h"

@implementation BBHeadlineChildLineCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews {
    
    NSLog(@"%lf,%lf",self.frame.size.width,self.frame.size.height);
    _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0 , self.frame.size.width, self.frame.size.height)];
    [self.contentView addSubview:_imageView];
    UIView *backGview = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 40 , self.frame.size.width, 40)];
    backGview.backgroundColor = [KblackColor colorWithAlphaComponent:0.2];
//    backGview.layer.cornerRadius = 4;
//    backGview.layer.masksToBounds = true;
    //设置所需的圆角位置以及大小
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:backGview.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(4, 4)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = backGview.bounds;
    maskLayer.path = maskPath.CGPath;
    backGview.layer.mask = maskLayer;
    [self.contentView addSubview:backGview];
    _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, self.frame.size.width - 32, 40)];
    _titleLab.textAlignment = NSTextAlignmentLeft;
    _titleLab.font = [UIFont systemFontOfSize:16];
    _titleLab.textColor = UIColorFromRGB(0xffffff);
    [backGview addSubview:_titleLab];
}

- (void)setHeadlineModel:(TTFModel *)headlineModel {
    
    _headlineModel = headlineModel;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:headlineModel.picpath]];
    _titleLab.text = headlineModel.text;
}

@end
