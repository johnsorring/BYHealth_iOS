//
//  BaseModel.h
//  BYHealth_iOS
//
//  Created by panshen on 2017/9/18.
//  Copyright © 2017年 panshen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

-(instancetype)initWithDict:(NSDictionary *)dic;

@end
