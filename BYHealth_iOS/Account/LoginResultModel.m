//
//  LoginResultModel.m
//  baobaotong
//
//  Created by lk on 2017/5/19.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "LoginResultModel.h"

@implementation LoginResultModel

+(NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"Vipinfo":@"vipinfo"};
}

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property
{
    if ([property.name isEqualToString:@"publisher"]) {
        if (oldValue == nil) return @"";

    } else if (property.type.typeClass == [NSDate class]) {
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"yyyy-MM-dd";
        return [fmt dateFromString:oldValue];
    }

    return oldValue;
}

@end
