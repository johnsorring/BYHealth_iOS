//
//  Account.m
//  baobaotong
//
//  Created by zzy on 16/1/15.
//  Copyright © 2016年 zzy. All rights reserved.
//

#import "Account.h"

static Account *account;
#define KAccountFileName @"KAccountFileName"

@implementation Account
+(instancetype)currentAccount{
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    account = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    if (!account) {
        account = [[Account alloc]init];
    }
    return account;
}
-(void)saveContentTime:(NSString *)contentTime
{
    self.contentTime = contentTime;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
- (void)saveHomePageHeadInfo:(NSDictionary *)result
{
    self.headInfo = result;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
- (void)saveHomepageVipInfo:(NSDictionary *)result
{
    self.vipInfo = result;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}

- (void)saveHomePgeNewComeInfo:(NSDictionary *)result
{
    self.NewComeInfo = result;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
- (void)saveLogin:(NSDictionary *)info
{
    self.userinfo = info;
    self.userId = info[@"userid"];
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
-(void)savePassVip:(BOOL)passvip
{
    self.passVip = passvip;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
-(void)saveShowDylog:(BOOL)isShowDylog
{
    self.isShowDylog = isShowDylog;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
- (void)saveUserName:(NSString*)userName {
    self.userName = userName;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
-(void)saveLoginNum:(NSString *)LogingNum
{
    self.LogingNum = LogingNum;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
-(void)saveMianLoginNum:(NSString *)MianLoginNum
{
    self.mianLoginNum = MianLoginNum;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
- (void)savePassword:(NSString*)password {
    self.password = password;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
- (void)savepassTime {
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    long long int date = (long long int)time;
    NSString * locationString = [NSString stringWithFormat:@"%lld",date];
    self.passTime = locationString;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
- (void)saveIsWXLogin:(BOOL)isWXLogin {
    self.isWXLogin = isWXLogin;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
- (void)saveWXName:(NSString*)wxName {
    self.wxName = wxName;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}

- (void)saveWeixinstatus:(NSString*)weixinstatus {
    self.weixinstatus = weixinstatus;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
-(void)saveissfzh:(NSString *)sfzh
{
    self.issfzh = sfzh;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
-(void)savecardKind:(NSString *)cardkind
{
    self.cardKind = cardkind;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}


- (void)saveVipInfo:(NSDictionary *)vipDict
{
    
    if (vipDict != nil || [vipDict class] != [NSNull class]) {
        
        NSString *dateStr = nil;
        if ([vipDict[@"deadline"] length] > 11 && [vipDict[@"deadline"] rangeOfString:@"fucking"].location == NSNotFound) {
            NSString *date = [vipDict[@"deadline"] substringToIndex:11];
            dateStr = [date stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
        }
        NSLog(@"date type -----------  %@",dateStr);
        self.vipName = vipDict[@"viptypename"];
        self.vipType = [NSString stringWithFormat:@"%@",vipDict[@"viptype"]];
        self.vipDeadLine = dateStr;
        
        NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
        [NSKeyedArchiver archiveRootObject:self toFile:filePath];
    }
}
-(void)DeletePassWord
{
    self.password = nil;
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    [NSKeyedArchiver archiveRootObject:self toFile:filePath];
}
- (BOOL)isLogin
{
    if (self.userId) {
        return YES;
    }
    return NO;
}
- (void)logOut
{
    
    //清除缓存
    self.userinfo = nil;
    self.passVip = NO;
    self.userId = nil;
    self.userName = nil;
    self.password = nil;
    self.isWXLogin = NO;
    self.wxName = nil;
    self.weixinstatus = nil;
    self.issfzh = nil;
    self.cardKind = nil;
    self.passTime = nil;
    self.LogingNum = nil;
    self.mianLoginNum = nil;
    self.vipName = nil;
    self.vipType = nil;
    self.vipDeadLine = nil;
    self.headInfo = nil;
    self.vipInfo = nil;
    self.NewComeInfo = nil;
    self.isShowDylog = NO;
    //删除归档文件
//        NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
//        NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
//        NSFileManager *fileManager = [NSFileManager defaultManager];
//        NSError *error;
//        [fileManager removeItemAtPath:filePath error:&error];
}
-(void)deleteMessage
{
    //清除缓存
    self.userinfo = nil;
    self.userId = nil;
    self.userName = nil;
    self.password = nil;
    self.isWXLogin = NO;
    self.wxName = nil;
    self.weixinstatus = nil;
    self.issfzh = nil;
    self.cardKind = nil;
    self.passTime = nil;
    self.LogingNum = nil;
    self.mianLoginNum = nil;
    self.vipName = nil;
    self.vipType = nil;
    self.vipDeadLine = nil;
    self.passVip = NO;
    self.headInfo = nil;
    self.vipInfo = nil;
    self.NewComeInfo = nil;
    self.isShowDylog = NO;
    //    删除归档文件
    NSString *docments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [docments stringByAppendingPathComponent:KAccountFileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    [fileManager removeItemAtPath:filePath error:&error];
}
#pragma nark --coding
-(void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.userinfo forKey:@"userinfo"];
    [coder encodeObject:self.userId forKey:@"userid"];
    [coder encodeObject:self.userName forKey:@"username"];
    [coder encodeObject:self.password forKey:@"password"];
    [coder encodeBool:self.isWXLogin forKey:@"iswxlogin"];
    [coder encodeBool:self.passVip forKey:@"passVip"];
    [coder encodeBool:self.isShowDylog forKey:@"isShowDylog"];
    
    [coder encodeObject:self.wxName forKey:@"wxname"];
    [coder encodeObject:self.weixinstatus forKey:@"weixinstatus"];
    [coder encodeObject:self.issfzh forKey:@"issfzh"];
    [coder encodeObject:self.cardKind forKey:@"cardkind"];
    [coder encodeObject:self.passTime forKey:@"currentTime"];
    [coder encodeObject:self.LogingNum forKey:@"LogingNum"];
    [coder encodeObject:self.mianLoginNum forKey:@"mianLoginNum"];
    [coder encodeObject:self.vipName forKey:@"vipName"];
    [coder encodeObject:self.vipType forKey:@"vipType"];
    [coder encodeObject:self.vipDeadLine forKey:@"vipDeadLine"];
    [coder encodeObject:self.headInfo forKey:@"headInfo"];
    [coder encodeObject:self.vipInfo forKey:@"vipInfo"];
    [coder encodeObject:self.NewComeInfo forKey:@"NewComeInfo"];
    [coder encodeObject:self.contentTime forKey:@"contentTime"];
}

-(id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self) {
        
        self.userinfo = [coder decodeObjectForKey:@"userinfo"];
        self.userId = [coder decodeObjectForKey:@"userid"];
        self.userName = [coder decodeObjectForKey:@"username"];
        self.password = [coder decodeObjectForKey:@"password"];
        self.isWXLogin = [coder decodeBoolForKey:@"iswxlogin"];
        self.isShowDylog = [coder decodeBoolForKey:@"isShowDylog"];
        self.wxName = [coder decodeObjectForKey:@"wxname"];
        self.weixinstatus = [coder decodeObjectForKey:@"weixinstatus"];
        self.issfzh = [coder decodeObjectForKey:@"issfzh"];
        self.cardKind = [coder decodeObjectForKey:@"cardkind"];
        self.passTime = [coder decodeObjectForKey:@"currentTime"];
        self.LogingNum = [coder decodeObjectForKey:@"LogingNum"];
        self.mianLoginNum = [coder decodeObjectForKey:@"mianLoginNum"];
        self.vipName = [coder decodeObjectForKey:@"vipName"];
        self.vipType = [coder decodeObjectForKey:@"vipType"];
        self.vipDeadLine = [coder decodeObjectForKey:@"vipDeadLine"];
        self.passVip = [coder decodeBoolForKey:@"passVip"];
        self.headInfo = [coder decodeObjectForKey:@"headInfo"];
        self.vipInfo = [coder decodeObjectForKey:@"vipInfo"];
        self.NewComeInfo = [coder decodeObjectForKey:@"NewComeInfo"];
        if ([coder decodeObjectForKey:@"contentTime"] == [NSNull class] || [[coder decodeObjectForKey:@"contentTime"] length] == 0) {
            self.contentTime = @"";
        }else
        {
           self.contentTime = [coder decodeObjectForKey:@"contentTime"];
        }
    }
    return self;
}

@end
