//
//  Vipinfo.h
//  baobaotong
//
//  Created by lk on 2017/5/19.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vipinfo : NSObject
@property (nonatomic, strong) NSString * deadline;
@property (nonatomic, assign) NSInteger remainingdays;
@property (nonatomic, assign) NSInteger vipstate;
@property (nonatomic, assign) NSInteger viptype;
@property (nonatomic, strong) NSString * viptypename;
@end
