//
//  NSObject+UserInfObject.m
//  baobaotong
//
//  Created by lk on 2017/6/21.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "NSObject+UserInfObject.h"
#import <objc/runtime.h>
#import "Account.h"

@implementation NSObject (UserInfObject)


-(NSString *)native_userid
{
    NSLog(@"%@",[Account currentAccount].userinfo[@"userid"]);
    NSString *native = [Account currentAccount].userinfo[@"userid"];
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_userid = native;
   return objc_getAssociatedObject(self, @selector(native_userid));
}

-(NSString *)native_useridForUTF8
{
    
    NSString *native = [[Account currentAccount].userinfo[@"userid"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (native == nil  || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_useridForUTF8 = native;
    return objc_getAssociatedObject(self, @selector(native_useridForUTF8));
}

-(NSString *)native_cardno
{
    
   NSString *native  = [Account currentAccount].userinfo[@"usersfzh"];
    if (native == nil  || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_cardno = native;
    return objc_getAssociatedObject(self, @selector(native_cardno));
}

-(NSString *)native_cardnoForUTF8
{
    
    NSString *native = [[Account currentAccount].userinfo[@"usersfzh"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_cardnoForUTF8 = native;
    return objc_getAssociatedObject(self, @selector(native_cardnoForUTF8));
}

-(NSString *)native_nickname
{
    NSString *native = [Account currentAccount].userinfo[@"nickname"];
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_nickname = native;
    return objc_getAssociatedObject(self, @selector(native_nickname));
}

-(NSString *)native_username
{
    NSString *native = [Account currentAccount].userinfo[@"username"];
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_username  = native;
    return objc_getAssociatedObject(self, @selector(native_username));
}

-(NSString *)native_personname
{
    NSString *native = [Account currentAccount].userinfo[@"personname"];
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_personname = native;
    return objc_getAssociatedObject(self, @selector(native_personname));
}

-(NSString *)native_identitycard
{
    NSString *cardKind = [Account currentAccount].cardKind;
    NSString *native = [Account currentAccount].userinfo[@"usersfzh"];
    if ([cardKind isEqualToString:@"未知"] || native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_identitycard = native;
    return objc_getAssociatedObject(self, @selector(native_identitycard));
}

-(NSString *)native_userphone
{
    NSString *native = [Account currentAccount].userinfo[@"userphone"];
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {native = @"";}
    self.native_userphone = native;
    return objc_getAssociatedObject(self, @selector(native_userphone));
}

-(NSString *)native_certtype
{
    NSString *native = [Account currentAccount].userinfo[@"certtype"];
    if (native == nil || [native isKindOfClass:[NSNull class]]  || native.length <= 0) {
        native = @"";
    }
    self.native_certtype = native;
    return objc_getAssociatedObject(self, @selector(native_certtype));
}

-(NSString *)native_profile
{
    NSString *native = [Account currentAccount].userinfo[@"profile"];
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_profile = native;
    return objc_getAssociatedObject(self, @selector(native_profile));
}

-(NSString *)native_productname
{
    NSString *native = [Account currentAccount].userinfo[@"productname"];
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_productname = native;
    return objc_getAssociatedObject(self, @selector(native_productname));
}

-(NSString *)native_logingNum
{
    NSString *native = [Account currentAccount].LogingNum;
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_logingNum = native;
    return objc_getAssociatedObject(self, @selector(native_logingNum));
}


-(NSString *)native_mianLoginNum
{
    NSString *native = [Account currentAccount].mianLoginNum;
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_mianLoginNum = native;
    return objc_getAssociatedObject(self, @selector(native_mianLoginNum));
}


-(NSDictionary *)native_userinfo
{
    NSDictionary *native = [Account currentAccount].userinfo;
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.count <= 0) {
        native = [NSDictionary dictionary];
    }
    self.native_userinfo = native;
    return objc_getAssociatedObject(self, @selector(native_userinfo));
}

-(NSString *)native_password
{
    NSString *native = [Account currentAccount].password;
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_password = native;
    return objc_getAssociatedObject(self, @selector(native_password));
}

-(NSString *)native_passTime
{
    NSString *native = [Account currentAccount].passTime;
    if (native == nil || [native isKindOfClass:[NSNull class]] || native.length <= 0) {
        native = @"";
    }
    self.native_passTime = native;
    return objc_getAssociatedObject(self, @selector(native_passTime));
}


//set方法
-(void)setNative_userid:(NSString *)native_userid{
    objc_setAssociatedObject(self, @selector(native_userid), native_userid, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_useridForUTF8:(NSString *)native_useridForUTF8{
    objc_setAssociatedObject(self, @selector(native_useridForUTF8), native_useridForUTF8, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_cardno:(NSString *)native_cardno
{
    objc_setAssociatedObject(self, @selector(native_cardno), native_cardno, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_cardnoForUTF8:(NSString *)native_cardnoForUTF8
{
    objc_setAssociatedObject(self, @selector(native_cardnoForUTF8), native_cardnoForUTF8, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_nickname:(NSString *)native_nickname
{
    objc_setAssociatedObject(self, @selector(native_nickname), native_nickname, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_personname:(NSString *)native_personname
{
    objc_setAssociatedObject(self, @selector(native_personname), native_personname, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_username:(NSString *)native_username
{
    objc_setAssociatedObject(self, @selector(native_username), native_username, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_identitycard:(NSString *)native_identitycard
{
    objc_setAssociatedObject(self, @selector(native_identitycard), native_identitycard, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_userphone:(NSString *)native_userphone
{
    objc_setAssociatedObject(self, @selector(native_userphone), native_userphone, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_certtype:(NSString *)native_certtype
{
    objc_setAssociatedObject(self, @selector(native_certtype), native_certtype, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_profile:(NSString *)native_profile
{
    objc_setAssociatedObject(self, @selector(native_profile), native_profile, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_productname:(NSString *)native_productname
{
   objc_setAssociatedObject(self, @selector(native_productname), native_productname, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_logingNum:(NSString *)native_logingNum
{
    objc_setAssociatedObject(self, @selector(native_logingNum), native_logingNum, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_userinfo:(NSDictionary *)native_userinfo
{
    objc_setAssociatedObject(self, @selector(native_userinfo), native_userinfo, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_mianLoginNum:(NSString *)native_mianLoginNum
{
    objc_setAssociatedObject(self, @selector(native_mianLoginNum), native_mianLoginNum, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_password:(NSString *)native_password
{
    objc_setAssociatedObject(self, @selector(native_password), native_password, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)setNative_passTime:(NSString *)native_passTime
{
    objc_setAssociatedObject(self, @selector(native_passTime), native_passTime, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
