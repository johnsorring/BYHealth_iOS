//
//  UserInfoModel.h
//  baobaotong
//
//  Created by lk on 2017/5/18.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginResultModel.h"

@interface UserInfoModel : NSObject
@property (nonatomic, strong) NSString * javaClass;
@property (nonatomic, strong) LoginResultModel * result; //result较多不能用result的model
@property (nonatomic, strong) NSString * resultdesc;
@property (nonatomic, assign) NSInteger resultstate;
-(void)userInfoModelSaveUserInfo;
@end
