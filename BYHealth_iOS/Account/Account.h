//
//  Account.h
//  baobaotong
//
//  Created by zzy on 16/1/15.
//  Copyright © 2016年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 用户信息的存储单例
 
 包括用户的登录信息
 
 以及其他页面的信息（目前只做了首页的页面请求）
 */
@interface Account : NSObject

@property (nonatomic, strong) NSDictionary *userinfo;//总的信息
@property (nonatomic, copy) NSString *userId;//userid
@property (nonatomic, copy) NSString *userName;//账号
@property (nonatomic, copy) NSString *password;//密码
@property (nonatomic, copy) NSString *passTime; //当前登录的时间
@property (nonatomic, assign) BOOL isWXLogin;//用于微信绑定
@property (nonatomic, copy) NSString *wxName;//微信昵称
@property (nonatomic, copy) NSString *weixinstatus;//是否绑定微信的状态
@property (nonatomic, copy) NSString *issfzh;
@property (nonatomic, copy) NSString *cardKind;
@property (nonatomic,copy) NSString *LogingNum;
@property (nonatomic,copy) NSString *mianLoginNum;

@property (nonatomic,copy) NSString *vipName;
@property (nonatomic,copy) NSString *vipType;
@property (nonatomic,copy) NSString *vipDeadLine;
@property(nonatomic,assign)BOOL passVip;
@property (nonatomic, strong) NSDictionary *headInfo;//首页基本信息滚动视图
@property (nonatomic, strong) NSDictionary *vipInfo;//首页基本信息我的课程等一栏
@property (nonatomic, strong) NSDictionary *NewComeInfo;//首页新手专区基本信息
@property (nonatomic, copy) NSString *contentTime;//首页新手专区基本信息
@property (nonatomic,assign)BOOL isShowDylog;

+(instancetype)currentAccount;

//loggin result
- (void)saveLogin:(NSDictionary *)info;
- (void)savePassword:(NSString*)password;
- (void)saveUserName:(NSString*)userName;
- (void)savepassTime;
- (void)saveIsWXLogin:(BOOL)isWXLogin;
- (void)saveWXName:(NSString*)wxName;
- (void)saveContentTime:(NSString *)contentTime;
- (void)saveWeixinstatus:(NSString*)weixinstatus;
- (void)saveissfzh:(NSString *)sfzh;
- (void)savecardKind:(NSString *)cardkind;
- (void)saveVipInfo:(NSDictionary *)vipDict;
- (void)savePassVip:(BOOL)passvip;

//home result
-(void)saveHomePageHeadInfo:(NSDictionary *)result;
-(void)saveHomepageVipInfo:(NSDictionary *)result;
-(void)saveHomePgeNewComeInfo:(NSDictionary *)result;

//首页是否弹框 ---如果是需要进入开屏广告页，先不弹，等回到首页再弹
-(void)saveShowDylog:(BOOL)isShowDylog;

- (BOOL)isLogin;
- (void)logOut;
-(void)saveLoginNum:(NSString *)LogingNum;
-(void)saveMianLoginNum:(NSString *)MianLoginNum;
-(void)deleteMessage;
-(void)DeletePassWord;

@end
