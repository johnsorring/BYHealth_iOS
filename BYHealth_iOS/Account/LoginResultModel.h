//
//  LoginResultModel.h
//  baobaotong
//
//  Created by lk on 2017/5/19.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vipinfo.h"
@interface LoginResultModel : NSObject

@property (nonatomic, strong) NSString * cardtype;
@property (nonatomic, strong) NSString * cardtypename;
@property (nonatomic, strong) NSString * certtype;
@property (nonatomic, strong) NSString * companyname;
@property (nonatomic, assign) BOOL isauthorised;
@property (nonatomic, strong) NSString * isopenmobilelogin;
@property (nonatomic, strong) NSString * isrealcardno;
@property (nonatomic, strong) NSString * managerzonename;
@property (nonatomic, strong) NSString * managerzoneno;
@property (nonatomic, strong) NSString * nickname;
@property (nonatomic, strong) NSString * personname;
@property (nonatomic, strong) NSString * productname;
@property (nonatomic, strong) NSString * profile;
@property (nonatomic, strong) NSString * usercomp;
@property (nonatomic, strong) NSString * userheadurl;
@property (nonatomic, strong) NSString * userid;
@property (nonatomic, strong) NSString * username;
@property (nonatomic, strong) NSString * userphone;
@property (nonatomic, strong) NSString * usersfzh;
@property (nonatomic, strong) NSString * userworkarea;
@property (nonatomic, strong) Vipinfo * vipinfo;
@property (nonatomic, strong) NSString * weixinnickname;
@property (nonatomic, strong) NSString * wenxinstatus;
@property (nonatomic, strong) NSString * apptoken;
@property (nonatomic, strong) NSString *fingerprint;
@property (nonatomic, strong) NSString *isindexadsshow;
@end
