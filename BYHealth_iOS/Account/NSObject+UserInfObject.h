//
//  NSObject+UserInfObject.h
//  baobaotong
//
//  Created by lk on 2017/6/21.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (UserInfObject)
@property (nonatomic, copy,   readonly)NSString * native_userid;//用户ID
@property (nonatomic, copy,   readonly)NSString * native_useridForUTF8;//用户id转utf8

@property (nonatomic, copy,   readonly)NSString * native_cardno; //身份证号
@property (nonatomic, copy,   readonly)NSString * native_cardnoForUTF8;//身份证号转utf8

@property (nonatomic, copy,   readonly)NSString * native_nickname;//昵称

@property (nonatomic, copy,   readonly)NSString * native_personname;//真实姓名

@property (nonatomic, copy,   readonly)NSString * native_username;//用户名

@property (nonatomic, copy,   readonly)NSString * native_identitycard;//证件号码

@property (nonatomic, copy,   readonly)NSString * native_userphone;//手机号码

@property (nonatomic, copy,   readonly)NSString * native_certtype;//执业信息

@property (nonatomic, copy,   readonly)NSString * native_profile;//个人简介

@property (nonatomic, copy,   readonly)NSString * native_productname;//产品方案

@property (nonatomic, copy,   readonly)NSString * native_logingNum;//登录num

@property (nonatomic, strong, readonly)NSDictionary * native_userinfo;//个人信息

@property (nonatomic, copy,   readonly)NSString * native_mianLoginNum;//免登录Num

@property (nonatomic, copy,   readonly)NSString * native_password; //密码

@property (nonatomic, copy,   readonly)NSString * native_passTime; //上次登录时间
@end
