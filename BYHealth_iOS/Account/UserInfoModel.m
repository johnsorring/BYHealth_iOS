//
//  UserInfoModel.m
//  baobaotong
//
//  Created by lk on 2017/5/18.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "UserInfoModel.h"

@implementation UserInfoModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"LoginResultModel":@"result"};
}

- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property
{
    if ([property.name isEqualToString:@"publisher"]) {
        if (oldValue == nil) return @"";
        
    } else if (property.type.typeClass == [NSDate class]) {
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"yyyy-MM-dd";
        return [fmt dateFromString:oldValue];
    }
    
    return oldValue;
}

-(void)userInfoModelSaveUserInfo
{
    NSString *str = self.result.userid;
    NSString *docpath = [NSString stringWithFormat:@"%@/Documents/%@",NSHomeDirectory(),str];
    NSString *CachesPath =[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *cachePath = [NSString stringWithFormat:@"%@/%@",CachesPath,str];
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:docpath isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) )
    {
        [fileManager createDirectoryAtPath:docpath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    isDir  = NO;
    existed = [fileManager fileExistsAtPath:cachePath isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) )
    {
        [fileManager createDirectoryAtPath:cachePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    [[Account currentAccount] savepassTime];
    [[Account currentAccount] saveLogin:[self.result mj_keyValues]];
    [[Account currentAccount] saveUserName:self.result.username];
    [[Account currentAccount] saveWeixinstatus:self.result.wenxinstatus];
    [[Account currentAccount] saveWXName:self.result.weixinnickname];
    [[Account currentAccount] saveissfzh:[NSString stringWithFormat:@"%@",self.result.isrealcardno]];
    [[Account currentAccount] saveVipInfo:[self.result.vipinfo mj_keyValues]];
    NSString * cardKind = [NSString stringWithFormat:@"%@",self.result.cardtypename];
    if (cardKind.length != 0) {
    }
    else
    {
        cardKind = @"未知";
    }
    [[Account currentAccount] savecardKind:cardKind];
}

@end
