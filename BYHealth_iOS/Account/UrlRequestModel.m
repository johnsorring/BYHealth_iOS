//
//  UrlRequestModel.m
//  baobaotong
//
//  Created by lk on 2017/6/9.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "UrlRequestModel.h"

@implementation UrlRequestModel
- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property
{
    if ([property.name isEqualToString:@"publisher"]) {
        if (oldValue == nil) return @"";
        
    } else if (property.type.typeClass == [NSDate class]) {
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"yyyy-MM-dd";
        return [fmt dateFromString:oldValue];
    }
    
    return oldValue;
}
@end
