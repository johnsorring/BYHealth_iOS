//
//  HttpManager.m
//  baobaotong
//
//  Created by zzy on 2016/02/23.
//  Copyright © 2016年 zzy. All rights reserved.
//

#import "HttpManager.h"

static HttpManager *httpManager;

@implementation HttpManager

-(id)init
{
    self = [super init];
    if(self)
    {
        _service = [[AFNetWorkService alloc] init];
    }
    
    return  self;
}

+ (HttpManager*)getHttpManager {
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        if (!httpManager) {
            httpManager = [[HttpManager alloc]init];
        }
    });
    return httpManager;
}

@end
