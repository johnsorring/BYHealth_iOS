//
//  NetJuge.m
//  baobaotong
//
//  Created by 李康 on 2016/12/9.
//  Copyright © 2016年 zzy. All rights reserved.
//

#import "NetJuge.h"
#import <AFNetworking.h>
typedef enum : NSUInteger {
    SGNetworkStatusNone = 0,
    SGNetworkStatusUkonow,
    SGNetworkStatus3G,
    SGNetworkStatus4G,
    SGNetworkStatusWifi,
}SGNetworkStatus;
@implementation NetJuge
+(void)getContentNetWork:(netblock)block
{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    __weak typeof (&*manager)weakmanager=manager;
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        [weakmanager stopMonitoring];
        block(status);
    }];
}

// 状态栏是由当前app控制的，首先获取当前app
- (SGNetworkStatus)netWorkDetailStatus{
    UIApplication *app = [UIApplication sharedApplication];
    UIView *statusBar = [app valueForKeyPath:@"statusBar"];
    UIView *foregroundView = [statusBar valueForKeyPath:@"foregroundView"];
    UIView *networkView = nil;
    for (UIView *childView in foregroundView.subviews) {
        if ([childView isKindOfClass:NSClassFromString(@"UIStatusBarDataNetworkItemView")]) {
            networkView = childView;
        }
    }
    SGNetworkStatus status = SGNetworkStatusNone;
    if (networkView) {
        int netType = [[networkView valueForKeyPath:@"dataNetworkType"]intValue];
        switch (netType) {
            case 0:
                status = SGNetworkStatusNone;
                break;
            case 1://实际上是2G
                status = SGNetworkStatusUkonow;
                break;
            case 2:
                status = SGNetworkStatus3G;
                break;
            case 3:
                status = SGNetworkStatus4G;
                break;
            case 5:
                status = SGNetworkStatusWifi;
                break;
            default:
                status = SGNetworkStatusUkonow;
                break;
        }
    }
    return status;
}
@end
