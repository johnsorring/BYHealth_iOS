//
//  NetJuge.h
//  baobaotong
//
//  Created by 李康 on 2016/12/9.
//  Copyright © 2016年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^netblock)(AFNetworkReachabilityStatus afstatus);
@interface NetJuge : NSObject
+(void)getContentNetWork:(netblock)block;
@end
