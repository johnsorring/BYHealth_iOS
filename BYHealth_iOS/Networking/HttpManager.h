//
//  HttpManager.h
//  baobaotong
//
//  Created by zzy on 2016/02/23.
//  Copyright © 2016年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetWorkService.h"

@interface HttpManager : NSObject

//调用服务对象
@property(nonatomic,retain)AFNetWorkService *service;

+ (HttpManager*)getHttpManager;

@end
