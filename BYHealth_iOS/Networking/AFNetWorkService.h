//
//  AFNetWorkService.h
//  TBRJL
//
//  Created by zzy on 16/01/16.
//  Copyright (c) 2016年 zzy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpRequestInfo.h"

typedef void(^RequestCompelete)(id _Nullable
);
@interface AFNetWorkService : NSObject
@property(nonatomic,retain)id _Nullable result;
//设置获取cookID的服务参数对象
@property(nonatomic,retain)HttpRequestInfo * _Nullable requestInfo;
//版本
@property(nonatomic,copy)NSString * _Nullable versionStr;
//调用登录次数，防止无限循环
@property(nonatomic,assign)int count;
@property (nonatomic,copy)NSString * _Nullable token;

-(void)requestWithServiceIP:(NSString *_Nullable) serviceIP ServiceName:(NSString *_Nullable)serviceName
                       params:(NSMutableDictionary *_Nullable)params
                       httpMethod:(NSString *_Nullable)httpMethod
                       resultIsDictionary:(BOOL)resultIsDictionary
                       completeBlock:(RequestCompelete _Nullable )block;
//上传多个文件
- (void)uploadMutableimgWithWithServiceIP:(NSString *_Nullable)serviceIP ServiceName:(NSString *_Nullable)serviceName  Withparameter:(NSDictionary *_Nullable)parameter imagesarray:(NSMutableArray *_Nullable)imgarray progress:(void (^_Nullable)(NSProgress * _Nullable progress))progress success:(void (^_Nullable)())success fail:(void (^_Nullable)())fail;

-(NSString*_Nullable)objectToJson:(NSObject *_Nullable)object;

@end
