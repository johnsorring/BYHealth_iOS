//
//  LRTabBarController.m
//  CarRecordCarOwner
//
//  Created by Mag1cPanda on 2017/3/14.
//  Copyright © 2017年 Mag1cPanda. All rights reserved.
//

#import "LRTabBarController.h"
#import "LRNavigationController.h"
#import "HomeViewController.h"
#import "CourseViewController.h"
#import "RecordViewController.h"
#import "MineViewController.h"

@interface LRTabBarController ()


@end

@implementation LRTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    
    HomeViewController *vc1 = [HomeViewController new];
    vc1.tabBarItem.title = @"首页";
    vc1.tabBarItem.image = [UIImage imageNamed:@"icon_theater"];
    
    CourseViewController *vc2 = [CourseViewController new];
    vc2.tabBarItem.title = @"课程";
    vc2.tabBarItem.image = [UIImage imageNamed:@"icon_movie"];
    
//    RecordViewController *vc3 = [RecordViewController new];
//    vc3.tabBarItem.title = @"客户档案";
//    vc3.tabBarItem.image = [UIImage imageNamed:@"icon_found"];
    
    MineViewController *vc4 = [MineViewController new];
    vc4.tabBarItem.title = @"我的";
    vc4.tabBarItem.image = [UIImage imageNamed:@"icon_mine"];
    

    LRNavigationController *nav1 = [[LRNavigationController alloc] initWithRootViewController:vc1];
    LRNavigationController *nav2 = [[LRNavigationController alloc] initWithRootViewController:vc2];
//    LRNavigationController *nav3 = [[LRNavigationController alloc] initWithRootViewController:vc3];
    LRNavigationController *nav4 = [[LRNavigationController alloc] initWithRootViewController:vc4];
    
    self.viewControllers = @[nav1, nav2, nav4];
    

}


@end
