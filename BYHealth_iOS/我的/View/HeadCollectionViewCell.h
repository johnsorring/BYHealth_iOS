//
//  HeadCollectionViewCell.h
//  baobaotong
//
//  Created by 李康 on 2017/3/9.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeadCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong)UIImageView *backImgV;
@property (nonatomic,strong)UIImageView *headImgV;
@property (nonatomic,strong)UILabel *NameLab;
@property (nonatomic,strong)UIImageView *VipImgV;
@property (nonatomic,strong)UILabel *VipName;
//@property (nonatomic,strong)UILabel *MoneyLab;
//@property (nonatomic,strong)UIButton *settingBtn;
@property (nonatomic,strong)UIImageView * settingImg;
@property (nonatomic,strong)UIView *vipBackView;
@end
