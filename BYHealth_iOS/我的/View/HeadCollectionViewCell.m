//
//  HeadCollectionViewCell.m
//  baobaotong
//
//  Created by 李康 on 2017/3/9.
//  Copyright © 2017年 zzy. All rights reserved.
//

#import "HeadCollectionViewCell.h"

@implementation HeadCollectionViewCell
- (instancetype) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = KwhiteColor;
        [self addAllViews];
    }
    return self;
}

- (void)addAllViews{
    //头像
    CGFloat cellH = 160.0 * kDeviceWidth / 375.0;
    self.backImgV = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.backImgV.frame = CGRectMake(0, 0, kDeviceWidth, cellH);
    self.backImgV.image = [UIImage imageNamed:@"bg_my_user"];
    
    [self.contentView addSubview:self.backImgV];
    self.headImgV = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.headImgV.frame = CGRectMake(38.5, cellH - 64 - 31, 64, 64);
    self.headImgV.layer.cornerRadius = 32.0f;
    self.headImgV.layer.masksToBounds = YES;
    [self.backImgV addSubview:self.headImgV];
    
    self.vipBackView = [[UIView alloc] initWithFrame:CGRectZero];
    self.vipBackView.backgroundColor = UIColorFromRGB(0xe6e6e6);
    self.vipBackView.frame = CGRectMake(82.5, cellH - 20 - 32, 20, 20);
    self.vipBackView.layer.cornerRadius = 10.0f;
    self.vipBackView.layer.masksToBounds = YES;
    [self.backImgV addSubview:self.vipBackView];
    
    self.VipImgV = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.VipImgV.frame = CGRectMake(86.5, cellH - 32 - 4 - 12, 12, 12);
    self.VipImgV.image = [UIImage imageNamed:@"ic_my_member_normal"];
    [self.backImgV addSubview:self.VipImgV];
    
//    self.settingImg = [[UIImageView alloc] initWithFrame:CGRectZero];
//    [self.backImgV addSubview:self.settingImg];
//    
//    self.settingBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    self.settingBtn.frame = CGRectZero;
//    [self.backImgV addSubview:self.settingBtn];
    
    self.NameLab = [[UILabel alloc] initWithFrame:CGRectZero];
    self.NameLab.font = [UIFont systemFontOfSize:18];
    self.NameLab.textAlignment = NSTextAlignmentLeft;
    self.NameLab.textColor = UIColorFromRGB(0xffffff);
    self.NameLab.frame = CGRectMake(120.5, cellH - 62 - 25, kDeviceWidth - 120.5 - 20, 25);
    [self.backImgV addSubview:self.NameLab];
    
//    self.MoneyLab = [[UILabel alloc] initWithFrame:CGRectZero];
//    self.MoneyLab.frame = CGRectMake(120.5, cellH - 16.5 - 36.0, kDeviceWidth - 120.5 - 20, 16.5);
//    self.MoneyLab.font = [UIFont systemFontOfSize:12];
//    self.MoneyLab.textAlignment = NSTextAlignmentLeft;
//    self.MoneyLab.textColor = [UIColorFromRGB(0xffffff) colorWithAlphaComponent:0.6];
//    [self.backImgV addSubview:self.MoneyLab];
    
//    self.settingImg.frame = CGRectMake(kDeviceWidth - 16 - 24, 32, 24, 24);
//    self.settingImg.image = [UIImage imageNamed:@"ic_my_setting"];
//    
//    self.settingBtn.frame = CGRectMake(kDeviceWidth - 48, 16, 48, 48);
    
}

@end
