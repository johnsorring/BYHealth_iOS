//
//  MineViewController.m
//  BYHealth_iOS
//
//  Created by panshen on 2017/9/18.
//  Copyright © 2017年 panshen. All rights reserved.
//

#import "MineViewController.h"
#import "HeadCollectionViewCell.h"

#define MineHead @"MineHead"

@interface MineViewController ()
<UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout,
UIGestureRecognizerDelegate>

@property (nonatomic,strong)UICollectionView *colMain;
@property (nonatomic,strong)UIButton *settingBtn;
@property (nonatomic,strong)UIImageView * settingImg;

@end

@implementation MineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navBarView.hidden = YES;
    
    [IPDetector creatScreenAnalytics:@"我的"];
    
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.itemSize = CGSizeMake(kDeviceWidth/4.0, 93);
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    _colMain = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, KDeviceHeight - 44) collectionViewLayout:layout];
    _colMain.backgroundColor = KbackColor;
    _colMain.delegate = self;
    _colMain.showsVerticalScrollIndicator = NO;
    _colMain.showsHorizontalScrollIndicator = NO;
    _colMain.dataSource = self;
//    _colMain.mj_header = [NewRefresh headerWithRefreshingTarget:self refreshingAction:@selector(homeHeaderRereshing)];
    [self.view addSubview:_colMain];
    
    self.settingImg = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.settingBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.settingBtn.frame = CGRectZero;
    self.settingImg.frame = CGRectMake(kDeviceWidth - 16 - 24, 32, 24, 24);
    self.settingImg.image = [UIImage imageNamed:@"ic_my_setting"];
    self.settingBtn.frame = CGRectMake(kDeviceWidth - 48, 16, 48, 48);
    [self.settingBtn addTarget:self action:@selector(setting:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.settingBtn];
    [self.view addSubview:self.settingImg];

    
    [self.colMain registerClass:[HeadCollectionViewCell class] forCellWithReuseIdentifier:MineHead];//这种是没有xib的cell,有xib要用nib注册
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HeadCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MineHead forIndexPath:indexPath];
    [cell.headImgV sd_setImageWithURL:[NSURL URLWithString:[Account currentAccount].userinfo[@"userheadurl"]] placeholderImage:[UIImage imageNamed:@"face"]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake( kDeviceWidth, 160.0 * kDeviceWidth / 375.0);
}

@end
